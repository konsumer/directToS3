This is an ultra-simplified direct-to-S3 uploader.

### installation

```sh
npm i
```

### usage

```sh
export AWS_ACCESS_KEY_ID=<YOURS>
export AWS_SECRET_ACCESS_KEY=<YOURS>
export S3_BUCKET=<YOURS>

npm start
```

### CORS config

Put this under "Permissions" / "CORS Configuration" on S3:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
   <CORSRule>
        <AllowedOrigin>*</AllowedOrigin>
        <AllowedMethod>GET</AllowedMethod>
        <AllowedMethod>POST</AllowedMethod>
        <AllowedMethod>PUT</AllowedMethod>
        <AllowedHeader>*</AllowedHeader>
    </CORSRule>
</CORSConfiguration>
```