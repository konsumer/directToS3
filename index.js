const express = require('express')
const AWS = require('aws-sdk')
const shortid = require('shortid').generate

if (!process.env.AWS_ACCESS_KEY_ID || !process.env.AWS_SECRET_ACCESS_KEY || !process.env.S3_BUCKET) {
  throw new Error('you need to set S3_BUCKET, AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY')
}

const PORT = process.env.PORT || 3000
const S3_BUCKET = process.env.S3_BUCKET

const s3 = new AWS.S3()

const app = express()
app.use(express.static('public'))

app.get('/sign/:type/:file', (req, res) => {
  const name = `usernamehere/${shortid()}-${req.params.file}`
  const params = {
    Bucket: S3_BUCKET,
    Key: name,
    ACL: 'public-read',
    Expires: 10, // seconds
    ContentType: req.params.type
  }
  const signedUrl = s3.getSignedUrl('putObject', params)
  console.log({params, signedUrl, name})
  res.json({signedUrl, name})
})

app.listen(PORT)
console.log(`Listening on http://0.0.0.0:${PORT}`)
