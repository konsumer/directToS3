/* global fetch XMLHttpRequest */

// get signed S3 PUT URL
const getSig = file => fetch(`/sign/${encodeURIComponent(file.type)}/${encodeURIComponent(file.name)}`).then(r => r.json())

const handleProgress = progress => {
  const total = progress.reduce((p, v) => p + v, 0)
  console.log(total / progress.length)
}

const handleComplete = () => {
  console.log('complete')
}

window.addEventListener('load', () => {
  const file = document.querySelector('input')
  const button = document.querySelector('button')

  button.disabled = true
  file.addEventListener('change', () => {
    button.disabled = !file.files.length
  })

  button.addEventListener('click', async () => {
    if (file.files.length) {
      const files = [].slice.apply(file.files)
      const info = await Promise.all(files.map(getSig))
      const currentSize = new Array(files.length).fill(0)
      const finished = []
      info.forEach((item, u) => {
        // I use xhr instead of fetch to track progress
        const xhr = new XMLHttpRequest()
        xhr.upload.addEventListener('progress', e => {
          if (e.loaded) {
            currentSize[u] = e.loaded / e.total
          }
          handleProgress(currentSize)
        })
        xhr.upload.addEventListener('load', () => {
          finished.push(u)
          if (finished.length === info.length) {
            handleComplete()
          }
        })
        const file = files[u]
        xhr.open('PUT', item.signedUrl)
        xhr.overrideMimeType(file.type)
        xhr.send(file)
      })
    }
  })
})
